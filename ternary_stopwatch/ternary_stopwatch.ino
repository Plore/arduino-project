#include<string.h>
const int button1 = 2;
const int button2 = 4;
const int mypins_yellow[] = {11, 10, 9, 6};
const int mypins_red[] = {5, 3};

String DecToTern(int x)
{
  if(x == 0) return "0";
  if(x == 1) return "1";
  if(x == 2) return "2";
  return DecToTern(x / 3) + String(x % 3);
}

String Ternary(int x, int length)
{
  String res = DecToTern(x);
  while(res.length() < length)
    res = "0" + res;
  return res;
}

void lightpins(int timer)
{
  int timer_quick = timer % 10;
  int timer_slow = timer / 10;
    
  String onpins_y = Ternary(timer_slow, 4);
  String onpins_r = Ternary(timer_quick, 2);
  
  for (int i = 0; i < 4; i++)
  {
    if (onpins_y[i] == '0')
      analogWrite(mypins_yellow[i], 0);
    else if (onpins_y[i] == '1')
      analogWrite(mypins_yellow[i], 20);
    else if (onpins_y[i] == '2')
      analogWrite(mypins_yellow[i], 255);
  } 
  
  for(int i = 0; i < 2; i++)
  {
    if (onpins_r[i] == '0')
      analogWrite(mypins_red[i], 0);
    else if (onpins_r[i] == '1')
      analogWrite(mypins_red[i], 20);
    else
      analogWrite(mypins_red[i], 255);
  } 
}

void setup()
{
  for (int i = 0; i < 4; i++)
    pinMode(mypins_yellow[i], OUTPUT);
  
  for (int i = 0; i < 2; i++)
    pinMode(mypins_red[i], OUTPUT);
  
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  Serial.begin(9600);
}

boolean run = false;  
long t_push1 = 0L;
long t_push2 = 0L;
long t_start = 0L;
long timer = 0L;

void loop()
{  
  lightpins(timer);
  if (run == false)
  {
    if (digitalRead(button1) == LOW && millis() - t_push1 > 200)
    {
      run = true;
      t_push1 = millis();
      if (timer == 0)
        t_start = millis();
    }
  }
  else
  {
    timer = ((millis() - t_start) / 100) % 600;
    if (digitalRead(button1) == LOW && millis() - t_push1 > 200)
    {
      run = false;
      t_push1 = millis();
    }  
  }
  if(digitalRead(button2) == LOW && millis() - t_push2 > 500)
  {
      run = false;
      t_push2 = millis();
      timer = 0;
  }
}

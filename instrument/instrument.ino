#include "pitches.h"
const int sensorpin = 0;
const int buzzpin = 10;
const int buttonpin = 7;

int notes[4][8] = { {NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5},
                    {NOTE_C4, NOTE_D4, NOTE_E4, NOTE_FS4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5},
                    {NOTE_CS4, NOTE_D4, NOTE_E4, NOTE_FS4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_CS5},
                    {NOTE_CS4, NOTE_D4, NOTE_E4, NOTE_FS4, NOTE_GS4, NOTE_A4, NOTE_B4, NOTE_CS5}};
                  
void setup()
{
  pinMode(buzzpin, OUTPUT);
  pinMode(buttonpin, INPUT);
  Serial.begin(9600);
}

int sensorval = 0;
int pitch = 0;
long pushtime = 0;
int scale = 0;

void loop()
{
  int buttonstate = digitalRead(buttonpin);
  if (buttonstate == LOW && millis() - pushtime > 500)
  {
    scale = (scale + 1) % 4;
    Serial.println(scale);
    pushtime = millis();
  }
  sensorval = analogRead(sensorpin);
  if (sensorval == 0)
    noTone(buzzpin);
  else
  {
    pitch = map(sensorval, 0, 900, 0, 7);
    pitch = constrain(pitch, 0, 7);
    tone(buzzpin, notes[scale][pitch]);
  }
  delay(10);
}

#include<string.h>
const int button1 = 2;
const int button2 = 3;
const int mypins_yellow[] = {13, 12, 11, 10, 9, 8};
const int mypins_red[] = {7, 6, 5, 4};

String DecToBin(int x)
{
  if(x == 0) return "0";
  if(x == 1) return "1";
  if(x % 2 == 0) return DecToBin(x / 2) + "0";
  else return DecToBin(x / 2) + "1";
}

String Binary(int x, int length)
{
  String res = DecToBin(x);
  while(res.length() < length)
    res = "0" + res;
  return res;
}

void lightpins(int timer)
{
  int timer_quick = timer % 10;
  int timer_slow = timer / 10;
    
  String onpins_y = Binary(timer_slow, 6);
  String onpins_r = Binary(timer_quick, 4);
  
  for(int i = 0; i < 6; i++)
  {
    if(onpins_y[i] == '1')
      digitalWrite(mypins_yellow[i], HIGH);
    else
      digitalWrite(mypins_yellow[i], LOW);
  } 
  
  for(int i = 0; i < 4; i++)
  {
    if(onpins_r[i] == '1')
      digitalWrite(mypins_red[i], HIGH);
    else
      digitalWrite(mypins_red[i], LOW);
  }
}

void setup()
{
  for (int i = 0; i < 6; i++)
    pinMode(mypins_yellow[i], OUTPUT);
  
  for (int i = 0; i < 4; i++)
    pinMode(mypins_red[i], OUTPUT);
  
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  Serial.begin(9600);
  
}

boolean run = false;  
long t_push1 = 0L;
long t_push2 = 0L;
long t_start = 0L;
long timer = 0L;

void loop()
{  
  lightpins(timer);
  
  if (run == false)
  {
    if (digitalRead(button1) == LOW && millis() - t_push1 > 200)
    {
      run = true;
      t_push1 = millis();
      if (timer == 0)
      {
        t_start = millis();
//        Serial.println("resetting start time.");
      }
    }
  }
  else
  {
    timer = ((millis() - t_start) / 100) % 600;
       
    if (digitalRead(button1) == LOW && millis() - t_push1 > 200)
    {
      run = false;
      t_push1 = millis();
    }  
  }
  
  if(digitalRead(button2) == LOW && millis() - t_push2 > 500)
  {
      run = false;
      t_push2 = millis();
      Serial.println("Resetting timer.");
      Serial.println(t_push2);  
      timer = 0;
  }
}
